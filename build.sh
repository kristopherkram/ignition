for fcc in *.fcc; do
    ign=$(echo "$fcc" | cut -f 1 -d '.')
    docker run -i --rm quay.io/coreos/fcct:release --pretty --strict < $fcc > $ign.ign
done