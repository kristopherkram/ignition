# Fedora CoreOS Ignition configs

## Building
```Bash
.\build.sh
```

## Install
```Bash
sudo coreos-installer install /dev/vda --ignition-url https://gitlab.com/kristopherkram/ignition/-/raw/master/test.ign
```

## Download image
```bash
sudo docker run --privileged -ti --rm -v ${PWD}:/data quay.io/coreos/coreos-installer:release download -s stable -p metal -f iso  -C /data
```

## Virt-install
```bash
sudo docker run --privileged -ti --rm -v ${PWD}:/data quay.io/coreos/coreos-installer:release iso ignition embed -i /data/cd-install.ign /data/fedora-coreos-34.20210919.3.0-live.x86_64.iso -f
sudo cp fedora-coreos-34.20210919.3.0-live.x86_64.iso /var/lib/libvirt/images/
cp fedora-coreos-34.20210919.3.0-live.x86_64.iso /var/home/khkram_la/.local/share/libvirt/images
virt-install --connect="qemu:///session" --name=test --memory=2048 --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=/home/khkram_la/Documents/Source/ignition/test.ign" --cdrom="/home/khkram_la/Documents/Source/ignition/fedora-coreos-34.20210919.3.0-live.x86_64.iso" --disk="size=10"
```

virt-install --connect="qemu:///session" --name=test --memory=2048 --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=/var/lib/libvirt/images/cd-install.ign" --qemu-commandline="-fw_cfg name=coreos.inst.install_dev=/dev/vda"  --cdrom="/home/khkram_la/Documents/Source/ignition/fedora-coreos-34.20210919.3.0-live.x86_64.iso" --disk="size=10"


podman run -ti --rm -v ${PWD}:/data -w /data quay.io/coreos/coreos-installer:release download -s stable -p metal -f iso
